{
  open Lexing
  open Parser
  exception Error of string

  let next_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      { pos with pos_bol = lexbuf.lex_curr_pos;
                 pos_lnum = pos.pos_lnum + 1
      }
}
let chiffre = ['0'-'9']
let moin = '-'?chiffre+
rule token = parse
|"Noir" {NOIR}
|"Blanc" {BLANC}
|"Rouge" {ROUGE}
|"Vert" {VERT}
|"Bleu" {BLEU}
|"Jaune" {JAUNE}
|"Cyan" {CYAN}
|"ChangeCouleur" {CHANGE}

|"HautPinceau" { HAUTPINCEAU }
|"BasPinceau"  {  BASPINCEAU }
| [' ''\t'] { token lexbuf }
| "Debut" { BEGIN }
| eof   { EOF }
| '\n'   {next_line lexbuf; token lexbuf }
|"Var" { VAR }
| ';' {POINTVERGULE}
| '(' { LPARA }
| moin as i { NOMBRE (int_of_string i) }
| '0' {NOMBRE 0}
|"que" {QUE}
| ['a'-'z']['a'-'z' 'A'-'Z' '0'-'9']* as s { IDENT s}
| "Avance" {AVANCE}
| "Tourne" {TOURNE}
| '-' { MOINS}
| '+' { PLUS }
| '*' { MULT }
| "Si"  {SI}
|"Sinon" {SINON}
|"Alors"  {ALORS}
|"Tant" {TANT}
|"Faire" {FAIRE}
|'/' {DIV}
| ')' { RPARA }
| '=' { AFFECT } 
| "Fin"   { END }
| _   { raise (Error (Lexing.lexeme lexbuf)) }