let print_position outx lexbuf =
  Lexing.(
    let pos = lexbuf.lex_curr_p in
    Printf.fprintf outx "Ligne %d Col %d"
      pos.pos_lnum
      (pos.pos_cnum - pos.pos_bol + 1)
  )
let close_after_event () = ignore (Graphics.wait_next_event[])

let fct ins pos = let t  = Syntax.application [] ins pos false in
match t with 
_->()
let getInstruction a = 
match a with 
(_,a1)-> a1

let _ = print_string "Bienvenu ! note : les expressions doivent avoir des espaces entre elle (exemple : ( 1 + 2 ) et non (1+2), les noms de couleurs commencent par une majuscule ( exp ChangeCouleur Bleu; ), une boucle s'écrit Tant que et non tantque.\n";
let ch = open_in (Sys.argv.(1)) in
let lb = Lexing.from_channel ch 
in
  try
    let ast =
      Parser.s Lexer.token lb in
      let pos = Move.constructeur 0 0 90 in
      let ins = getInstruction ast in
       Typecheck.check_program ast;
        print_string "OK.\n";
        Graphics.open_graph " 1280x720";
        fct ins pos;
        close_after_event ();Graphics.close_graph()
    with
  | Lexer.Error msg ->
     Printf.fprintf stderr "%a: Lexer error reading %s\n" print_position lb msg;
     exit (-1)
  | Parser.Error ->
     Printf.fprintf stderr "%a: Syntax error\n" print_position lb;
     exit (-1)
  | Typecheck.Error s ->
  Printf.fprintf stderr "Type error: %s\n" s;
  exit (-1)
 
