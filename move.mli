type position ={
  x: float;      (** position x *)
  y: float;      (** position y *)
  a: int;        (** angle of the direction *)
}

type t = float

val degree_of_radian : float -> float  
val radian_of_degree : float -> float 
val in_canvas : position -> bool
val degree : float -> float  
val to_degree : t -> t  
val radian : float -> float  
val to_radian : float -> float  
val move_position : position -> int -> position  
val constructeur : int -> int -> int -> position  
val tourner : position -> int -> position  