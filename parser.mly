%token <int> NOMBRE
%token <string> IDENT
%token  VAR AVANCE TOURNE
%token POINTVERGULE 
%token HAUTPINCEAU BASPINCEAU
%token LPARA RPARA 
%token MULT DIV 
%token PLUS MOINS  
%token AFFECT BEGIN END EOF
%token SINON ALORS TANT SI FAIRE QUE
%token CHANGE MAGENTA CYAN JAUNE BLEU VERT ROUGE BLANC NOIR
%start <Syntax.program> s
%{ open Syntax %}
%%

s: p = program  EOF    {p}

program: ds=declaration  is=instruction   {(ds,is)}

declaration: 
  | VAR  id=IDENT  POINTVERGULE de=declaration  {Var (id,de)}
  | {Nonep}

instruction:
  | BASPINCEAU POINTVERGULE { BasPinceau} 
  | SI e=expression ALORS i=instruction SINON i1=instruction {Condition (e,i,i1)}
  | HAUTPINCEAU POINTVERGULE { HautPinceau }
  | TANT QUE e=expression FAIRE i = instruction {Boucle (e,i)}
  | id2=IDENT AFFECT e=expression POINTVERGULE {Affectation(id2,e)}
  | BEGIN bd=blocInstruction {Bi(bd)}
  | AVANCE a=expression POINTVERGULE {Avance (a)}
  | TOURNE t=expression POINTVERGULE {Tourne (t)}
  | CHANGE NOIR POINTVERGULE { Color (Noir) }
  | CHANGE BLANC POINTVERGULE { Color (Blanc) }
  | CHANGE BLEU POINTVERGULE { Color (Bleu) }
  | CHANGE JAUNE POINTVERGULE { Color (Jaune) }
  | CHANGE VERT POINTVERGULE { Color (Vert) }
  | CHANGE ROUGE POINTVERGULE { Color (Rouge) }
  | CHANGE CYAN POINTVERGULE { Color (Cyan) }
  | CHANGE MAGENTA POINTVERGULE { Color (Magenta) }

blocInstruction :
  |inst = instruction bd=blocInstruction {Block(inst,bd)}
  |END {Epsi}

expression:
  | s=NOMBRE es=expressionSuite {App1 (s,es)}
  | id=IDENT es=expressionSuite {App2 (id,es)}
  | LPARA e=expression RPARA es=expressionSuite {App3 (e,es)}


expressionSuite : 
  |PLUS e=expression {App4 (Plus,e)}
  |MOINS e=expression {App4 (Moins,e)}
  |DIV e=expression {App4 (Div,e)}
  |MULT e=expression {App4 (Mult,e)}
  |{None}
