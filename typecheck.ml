exception Error of string

open Syntax

let rec declaration_tolist s =
  match s with 
    Var(s,d) -> s :: declaration_tolist d
  | Nonep -> []

let rec check_double l = 
  match l with 
    h::rest -> if List.mem h rest then raise (Error ("Declared twice: "^h))
      else check_double rest
  |[]->()
    
let check_variable s = check_double (declaration_tolist s)    


let rec check_expression expr lis= 
  match expr with 
    App1 (_,es)-> check_expression_suite es lis  
  | App2 (s,es1) -> if List.mem s lis then check_expression_suite es1 lis 
      else raise (Error ("variable non declarer "))
  |App3 (excn, exsuit) -> 
      check_expression excn lis;
      check_expression_suite exsuit lis
and check_expression_suite exprs lis = 
  match exprs with 
  App4(_,esp) ->check_expression esp lis 
  |None-> () 


let rec check_instruction inst lis = 
  match inst with
    BasPinceau  -> ()
  | HautPinceau-> ()
  | Condition (e,i,i2)-> check_expression e lis ;
  check_instruction i lis ; check_instruction i2 lis 
  | Boucle (e1,i1)-> check_expression e1 lis ;
  check_instruction i1 lis
  | Color (_) ->  ()
  | Avance (expr1) ->check_expression expr1 lis ;
  | Tourne (expr2) ->check_expression expr2 lis
  |Affectation (s,expre5)-> 
  if (List.mem s lis) then check_expression expre5 lis
  | Bi  (bloc) -> check_instructionbloc bloc lis
and check_instructionbloc blok lis = 
  match blok with 
    Block (ins,bi) -> check_instruction ins lis ; check_instructionbloc bi lis 
    |Epsi -> ()

let check_program (dl,il) =
  check_variable dl; let a = declaration_tolist dl in check_instruction il a