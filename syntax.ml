(* Syntaxe abstraite *)
exception Error of string
(* Opérateurs binaires *)
type op = Plus | Mult | Moins | Div  


type color = Magenta | Cyan | Jaune | Bleu | Vert | Rouge | Blanc | Noir
(* Expressions *)

type expression = 
  |App1 of int * expressionSuite
  |App2 of string * expressionSuite 
  |App3 of  expression *  expressionSuite 
and expressionSuite = 
  | App4 of op * expression
  | None


let coleurvalue m = 
match m with
Magenta-> Graphics.magenta 
| Cyan -> Graphics.cyan
| Jaune -> Graphics.yellow
| Bleu -> Graphics.blue
| Vert -> Graphics.green
| Rouge -> Graphics.red
| Blanc -> Graphics.white
| Noir -> Graphics.black

let changecolor m = 
let v = coleurvalue m in Graphics.set_color v


let rec calcule_expression list_value expression = 
  match expression with 
    App1(i,expsuite) -> calcule_expression_suite list_value i expsuite 
  |App2(s,expsuite) -> if List.mem_assoc s list_value then
        let t = List.assoc s list_value  
        in calcule_expression_suite list_value t expsuite
      else raise (Error ("Error valeur non initialiser"))
  |App3(e,es)->
      let t = calcule_expression list_value e
      in calcule_expression_suite list_value t es
and calcule_expression_suite list_value value expr_suite= 
  match expr_suite with 
    App4(Plus,expre)-> value + calcule_expression list_value expre 
  |App4(Moins,expre)-> value - calcule_expression list_value expre
  |App4(Mult,expre)-> value * calcule_expression list_value expre
  |App4(Div,expre)-> let t = calcule_expression list_value expre in
      if t == 0 then raise (Error ("Div 0 Error")) 
      else value / t
  |None -> value



(* Instructions *)
type instruction =
  | Avance of expression 
  | Tourne of expression 
  | Affectation of string * expression
  | Boucle of expression * instruction 
  | Condition of expression * instruction * instruction 
  | Color of color
  | BasPinceau   
  | HautPinceau 
  | Bi of  blocInstruction  
and blocInstruction = 
  |Block of instruction * blocInstruction 
  | Epsi
(* Les types utilsés dans les décalarations de vraiables *)
let rec application list_value instruction position bo =
  match  instruction with 
    Avance (exp) -> let t = calcule_expression list_value exp in
      let pos = Move.move_position position t in
      let x1,y1 =int_of_float (pos.x) ,int_of_float (pos.y) 
      in  
      if Move.in_canvas pos && bo then
        begin
          Graphics.lineto x1 y1;list_value ,pos ,bo
        end
      else if Move.in_canvas pos && bo==false
      then
        begin
          Graphics.moveto x1 y1 ;list_value,pos,bo
        end 
      else 
      begin 
        raise (Error ("Curser hors canvas"))
      end
  | Tourne (exp1)->  
      let t = calcule_expression list_value exp1 in
      (list_value,Move.tourner position t , bo)
  | Color (co) -> changecolor co ; list_value,position,bo
  |BasPinceau ->  (list_value,position,true)
  |HautPinceau -> (list_value,position,false)
  |Bi (block) -> application_suite list_value block position bo
  |Condition (ex1,i1,i2)-> if calcule_expression list_value ex1 == 0 
      then  application list_value i2 position bo 
      else application list_value i1 position bo
  | Boucle (ex,ins) -> if calcule_expression list_value ex == 0 
      then  (list_value,position,bo)
      else let  b1 ,b2 , b3 = application list_value ins position bo 
        in let is = Boucle(ex,ins) in  
        application b1 is b2 b3 
  |Affectation (st , i1) ->if List.mem_assoc st list_value 
      then 
        let t = calcule_expression list_value i1 in 
        let ll = List.remove_assoc st list_value in (ll@[(st,t)],position,bo)
      else let t = calcule_expression list_value i1 in
        (list_value@[(st,t)],position,bo)
and application_suite list_value insuite posit boo  = 
  match insuite with
    Block(i1, i2)-> let a ,b ,c = application list_value i1 posit boo 
      in application_suite a i2 b c  
  |Epsi -> (list_value, posit ,boo)

(* Declaration d'un variable *)
type declaration = 
  |Var of string  * declaration
  | Nonep

(* Programm complèt *)
type program = declaration * instruction 


