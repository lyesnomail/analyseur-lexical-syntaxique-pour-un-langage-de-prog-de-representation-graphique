type position = {
  x: float;      (** position x *)
  y: float;      (** position y *)
  a: int;        (** angle of the direction *)
}

type t = float

let pi_h = acos 0. 
let degree_of_radian x = x *. 90. /. pi_h 
let radian_of_degree x = x *. pi_h /. 90. 
let degree d:float = d
let to_degree a:t = a
let radian r = degree_of_radian r
let to_radian a = radian_of_degree a
let in_canvas (p:position) = (p.x < 1280. && p.y < 720.) && (p.x >= 0. && p.y >= 0.)
let move_position (p:position) (uniter : int) = 
  if sin(radian_of_degree (float_of_int (p.a))) = 1. 
  then {x = p.x ;y = p.y +. float_of_int(uniter) ; a=p.a}
  else if sin(radian_of_degree (float_of_int (p.a))) = -1.
  then {x = p.x ;y = p.y -. float_of_int(uniter) ; a=p.a}
  else if cos (radian_of_degree (float_of_int (p.a))) = 1. 
  then {x = p.x +. float_of_int(uniter) ;y = p.y  ; a=p.a}
  else if cos (radian_of_degree (float_of_int (p.a))) = -1. 
  then {x = p.x -. float_of_int(uniter) ;y = p.y  ; a=p.a}
  else let add_x = float_of_int(uniter)*.cos (radian_of_degree (float_of_int (p.a))) in 
    let add_y =float_of_int(uniter)*.sin(radian_of_degree (float_of_int (p.a)))
    in {x = p.x +. add_x ;y = p.y +. add_y ; a=p.a}


let constructeur a1 b1 c1  :position ={x =float_of_int(a1) ; y =float_of_int(b1); a =c1} 


let tourner (p :position) angle = {x =p.x ; y =p.y ; a = p.a + angle}
