exception Error of string
(* Opérateurs binaires *)
type op = Plus | Mult | Moins | Div  

type color = Magenta | Cyan | Jaune | Bleu | Vert | Rouge | Blanc | Noir
(* Expressions *)

type expression = 
  |App1 of int * expressionSuite
  |App2 of string * expressionSuite 
  |App3 of  expression *  expressionSuite 
and expressionSuite = 
  | App4 of op * expression
  | None
type instruction =
  | Avance of expression 
  | Tourne of expression 
  | Affectation of string * expression
  | Boucle of expression * instruction 
  | Condition of expression * instruction * instruction 
  | Color of color
  | BasPinceau   
  | HautPinceau 
  | Bi of  blocInstruction  
and blocInstruction = 
  |Block of instruction * blocInstruction 
  | Epsi

type declaration = 
  |Var of string  * declaration
  | Nonep
  
type program = declaration * instruction

val application :
  (string * int) list ->
  instruction -> Move.position -> bool -> (string * int) list * Move.position * bool

